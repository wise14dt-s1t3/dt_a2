-- Code belongs to TI3 DT
-- Stimuli Generator for ISE resp. XC2C256-CPLD related timing simulation just an example
--
-- HISTORY:
-- 1st release for WS 13/14
-- update for WS 14/15



library work;
    use work.all;
    
library ieee;
    use ieee.std_logic_1164.all;
    use ieee.std_logic_arith.all;       -- Entscheidung fuer "std_logic_arith" und gegen "numeric_std_unsigned"
    use ieee.std_logic_unsigned.all;    -- Entscheidung fuer "std_logic_arith" und gegen "numeric_std_unsigned"



-- sg ::= Stimuli Generator    
entity sg is
    port (
        data : out std_logic_vector( 3 downto 0);
        clk  : out std_logic;
        nres : out std_logic
    );--]port
end entity sg;



architecture beh of sg is
    
    -- Das Arbeiten mit 1/8 Takt-Zyklen ist eigentlich "uebertrieben",
    --   aber es erleichtert das Lesen der Waves
    --   und es bleibt auf den Stimuli Generator beschraenkt
    -- 8*2500ps = 20ns Takt-Periode => 50MHz Takt-Frequenz
    constant oneEigthClockCycle     : time  := 2500 ps;
    constant sevenEigthClockCycle   : time  := 7 * oneEigthClockCycle;
    constant halfClockCycle         : time  := 4 * oneEigthClockCycle;
    constant fullClockCycle         : time  := 8 * oneEigthClockCycle;
    
    
    signal   simulationRunning_s    : boolean  := true;
    
    signal   VDD                    : std_logic  := '1';
    signal   GND                    : std_logic  := '0';
    
    signal   nres_s                 : std_logic  := 'L';
    signal   clk_s                  : std_logic;
    
begin
    
    VDD <= '1';
    GND <= '0';
    
    
    
    resGen:                                             -- RESet GENerator
    process is
    begin
        nres_s <= '0';                                  -- set low active reset
        for i in 1 to 2 loop
            wait until '1'=clk_s and clk_s'event;       -- wait for rising clock edge
        end loop;
        -- since 2 rising clk edges have passed, synchronous reset must have been executed
        
        wait for oneEigthClockCycle;
        nres_s <= '1';                                  -- clear low active reset
        -- 1/8 clk period after rising clk edge reset is vanishing
        
        wait;
    end process resGen;
    --
    nres <= nres_s;
    
    
    
    
    
    clkGen:                                             -- CLocK GENerator
    process is
    begin
        clk_s <= '0';
        wait for fullClockCycle;
        while simulationRunning_s loop
            clk_s <= '1';
            wait for halfClockCycle;
            clk_s <= '0';
            wait for halfClockCycle;
        end loop;
        wait;
    end process clkGen;
    --
    clk <= clk_s;
    
    
    
    
    
    sg:                                                 -- Stimuli Generator
    process is
    begin
        simulationRunning_s <= true;
        
        if  nres_s/='0'  then  wait until nres_s='0';  end if;
        data <= (others=>'0');                          -- just set any defined data
        if  nres_s/='1'  then  wait until nres_s='1';  end if;
        --reset has passed
        
        
        
        -- give CPLD some time to wake up
        for  i in 1 to 10 loop
            wait until '1'=clk_s and clk_s'event;       -- wait for rising clock edge
        end loop;
        -- CPLD is configured and able to react
        
        
        
        -- "HIER" kommt der "eigentliche Test"
        -- Beispielhaft ein paar Testmuster erzeugen
        for i in 0 to 15 loop
            wait for oneEigthClockCycle;
            data <= conv_std_logic_vector( i, 4 );      -- "conv_std_logic_vector()" aus "std_logic_arith"
            wait until '1'=clk_s and clk_s'event;
        end loop;
        -- Der "eigentliche Test" ist nun zu Ende
        
        
        
        -- stop SG after 10 clk cycles - assuming computed data is stable afterwards
        for  i in 1 to 10 loop
            wait until '1'=clk_s and clk_s'event;       -- wait for rising clock edge
        end loop;
        simulationRunning_s <= false;                   -- stop clk generation
        --
        wait;
    end process sg;
    
end architecture beh;

