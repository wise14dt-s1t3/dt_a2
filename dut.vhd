-- Code belongs to EdJoPaTo

library work;
	use work.all;

library ieee;
	use ieee.std_logic_1164.all;


entity dut is
	port(
		rxdat : out std_logic_vector( 3 downto 0 );
		ok    : out std_logic;
		txdat : in  std_logic_vector( 3 downto 0 );
		clk   : in  std_logic;
		nres  : in  std_logic
	);
end entity dut;


architecture beh of dut is
	
	signal conn_cs : std_logic_vector( 4 downto 0);
	
	component sender
		port(
			i    : in std_logic_vector( 3 downto 0 );
			clk  : in std_logic;
			nres : in std_logic;

			o    : out std_logic_vector( 4 downto 0 )
	   );--]port
	end component sender;
	for all : sender use entity work.sender( arc );
	
	component receiver
		port(
			i    : in std_logic_vector( 4 downto 0 );
			clk  : in std_logic;
			nres : in std_logic;

			o    : out std_logic_vector( 3 downto 0 );
			ok   : out std_logic
	   );--]port
	end component receiver;
	for all : receiver use entity work.receiver( arc );
	
begin
	
	sender_i : sender
		port map (
			o    => conn_cs,
			i    => txdat,
			clk  => clk,
			nres => nres
		)--]port
	;--]sender_i
	
	receiver_i : receiver
		port map (
			i    => conn_cs,
			o    => rxdat,
			ok   => ok,
			clk  => clk,
			nres => nres
		)--]port
	;--]receiver_i
	
end architecture beh;

