-- Code belongs to EdJoPaTo

library work;
	use work.all;

library ieee;
	use ieee.std_logic_1164.all;


entity receiver is
	port (
		i    : in std_logic_vector( 4 downto 0 );
		clk  : in std_logic;
		nres : in std_logic;

		o    : out std_logic_vector( 3 downto 0 );
		ok   : out std_logic
	);
end entity receiver;



architecture arc of receiver is
	signal i_cs : std_logic_vector(4 downto 0) := (others => '0');
	signal parity_ns : std_logic := '1';
	signal ok_ns  : std_logic := '1';

	signal o_cs : std_logic_vector( 3 downto 0 ) := (others => '0');
	signal ok_cs : std_logic := '1'; 

	component epc
		port(
			i : in std_logic_vector( 3 downto 0);
			o  : out std_logic
	   );--]port
	end component epc;
	for all : epc use entity work.epc( arc );
	
begin

	o <= o_cs;
	ok <= ok_cs;

	epc_i : epc
		port map (
			o => parity_ns,
			i => i_cs(3 downto 0)
		)--]port
	;--]epc_i


	reg:
	process ( clk ) is
	begin
		if clk='1' and clk'event then
			if nres='0' then
				o_cs <= ( others => '0' );
				ok_cs <= '1';

				i_cs <= (4 => '1', others => '0');
			else
				o_cs <= i_cs(3 downto 0);
				ok_cs <= ok_ns;
				i_cs <= i;
			end if;
						
		end if;
	end process reg;

	isok:
	process ( parity_ns, i_cs ) is
		variable resu_v : std_logic;
	begin
		resu_v := parity_ns xnor i_cs(4);

		ok_ns <= resu_v;
	end process isok;
		
end architecture arc;

