-- Code belongs to EdJoPaTo

library work;
	use work.all;

library ieee;
	use ieee.std_logic_1164.all;


entity tb4dut is
end entity tb4dut;


architecture beh of tb4dut is
	
	signal i_ns : std_logic_vector( 3 downto 0);
	signal ok_cs : std_logic;
	signal o_cs  : std_logic_vector (3 downto 0);
	signal clk_s : std_logic;
	signal nres_s : std_logic;
	
	component sg
		port(
			data : out std_logic_vector( 3 downto 0);
			clk  : out std_logic;
			nres : out std_logic
		);--]port
	end component sg;
	for all : sg use entity work.sg( beh );
	
	component dut
		port(
			rxdat : out std_logic_vector( 3 downto 0 );
			ok    : out std_logic;
			txdat : in  std_logic_vector( 3 downto 0 );
			clk   : in  std_logic;
			nres  : in  std_logic
	   );--]port
	end component dut;
	for all : dut use entity work.dut( Structure );
	
begin
	
	sg_i : sg
		port map (
			data => i_ns,
			clk  => clk_s,
			nres => nres_s
		)--]port
	;--]sg_i
	
	dut_i : dut
		port map (
			rxdat => o_cs,
			ok    => ok_cs,
			txdat => i_ns,
			clk   => clk_s,
			nres  => nres_s
		)--]port
	;--]dut_i
	
end architecture beh;

