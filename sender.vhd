-- Code belongs to EdJoPaTo

library work;
	use work.all;

library ieee;
	use ieee.std_logic_1164.all;


entity sender is
	port (
		i    : in std_logic_vector( 3 downto 0 );
		clk  : in std_logic;
		nres : in std_logic;

		o    : out std_logic_vector( 4 downto 0 )
	);
end entity sender;



architecture arc of sender is
	signal i_cs : std_logic_vector(3 downto 0) := (others => '0');
	signal ok_ns  : std_logic := '1';

	signal o_cs : std_logic_vector( 4 downto 0 ) := (4 => '0', others => '0');

	component epc
		port(
			i : in std_logic_vector( 3 downto 0);
			o  : out std_logic
	   );--]port
	end component epc;
	for all : epc use entity work.epc( arc );
	
begin

	o <= o_cs;

	epc_i : epc
		port map (
			o => ok_ns,
			i => i_cs
		)--]port
	;--]epc_i


	reg:
	process ( clk ) is
	begin
		if clk='1' and clk'event then
			if nres='0' then
				o_cs <= ( others => '0' );
				o_cs(4) <= '1';

				i_cs <= (others => '0');
			else
				o_cs <= ok_ns & i_cs;
				i_cs <= i;
			end if;
						
		end if;
	end process reg;
		
end architecture arc;

