-- Code belongs to EdJoPaTo

library work;
	use work.all;

library ieee;
	use ieee.std_logic_1164.all;


entity epc is
	port (
		i : in std_logic_vector( 3 downto 0);
		o  : out std_logic
	);
end entity epc;



architecture arc of epc is
begin
	
	logic:
	process ( i ) is
		variable resu_v : std_logic;
	begin
		resu_v := not ((i(0) xor i(1)) xor (i(2) xor i(3)));
		
		o  <=  resu_v;
	end process logic;
		
end architecture arc;

