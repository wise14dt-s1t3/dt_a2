-- Code belongs to EdJoPaTo

library work;
	use work.all;

library ieee;
	use ieee.std_logic_1164.all;


entity tb4sender is
end entity tb4sender;


architecture beh of tb4sender is
	
	signal i_ns : std_logic_vector( 3 downto 0);
	signal o_cs  : std_logic_vector (4 downto 0);
	signal clk_s : std_logic;
	signal nres_s : std_logic;
	
	component sg
		port(
			data : out std_logic_vector( 3 downto 0);
			clk  : out std_logic;
			nres : out std_logic
		);--]port
	end component sg;
	for all : sg use entity work.sg( beh );
	
	component sender
		port(
			i    : in std_logic_vector( 3 downto 0 );
			clk  : in std_logic;
			nres : in std_logic;

			o    : out std_logic_vector( 4 downto 0 )
	   );--]port
	end component sender;
	for all : sender use entity work.sender( arc );
	
begin
	
	sg_i : sg
		port map (
			data => i_ns,
			clk  => clk_s,
			nres => nres_s
		)--]port
	;--]sg_i
	
	sender_i : sender
		port map (
			o    => o_cs,
			i    => i_ns,
			clk  => clk_s,
			nres => nres_s
		)--]port
	;--]sender_i
	
end architecture beh;

